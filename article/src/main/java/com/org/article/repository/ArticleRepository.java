package com.org.article.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.article.entity.Article;

@Repository
public interface ArticleRepository extends JpaRepository<Article, String> {

	Optional<Article> findBySlug(String slug);

}
