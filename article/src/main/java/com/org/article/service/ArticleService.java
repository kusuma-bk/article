package com.org.article.service;

import java.util.List;

import com.org.article.dto.ArticleRequestDto;
import com.org.article.entity.Article;
import com.org.article.exception.CustomException;

public interface ArticleService {

	public Article createArticle(ArticleRequestDto artcleRequestDto, String[] tags) throws CustomException;

	public List<Article> getAllArticles() throws CustomException;

	public Article getArticleById(String slug) throws CustomException;

	public String deleteArticleById(String slug) throws CustomException;

	public String updateArticle(Article article) throws CustomException;

}
