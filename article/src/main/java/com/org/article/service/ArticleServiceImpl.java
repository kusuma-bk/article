package com.org.article.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.article.dto.ArticleRequestDto;
import com.org.article.entity.Article;
import com.org.article.exception.CustomException;
import com.org.article.repository.ArticleRepository;

@Service
public class ArticleServiceImpl implements ArticleService {

	@Autowired
	ArticleRepository articleRepository;

	@Override
	public Article createArticle(ArticleRequestDto artcleRequestDto, String[] tags) throws CustomException {
		Article article = new Article();
		BeanUtils.copyProperties(artcleRequestDto, article);

		Optional<Article> optionalArticle = articleRepository.findBySlug(artcleRequestDto.getSlug());
		if (optionalArticle.isPresent()) {
			throw new CustomException("Article already present");
		}
		article.setCreatedAt(LocalDateTime.now());
		article.setFavorited(false);
		article.setFavoritesCount(0);
		article.setUpdatedAt(LocalDateTime.now());
		int i = tags.length;
		int n = ++i;
		String tagsIn[] = new String[n];
		if (tags.length != 0) {

			for (int cnt = 0; cnt < tags.length; cnt++) {

				tagsIn[cnt] = tags[cnt].toLowerCase();

			}
		}
		article.setTags(tagsIn);
		articleRepository.save(article);
		return article;
	}

	@Override
	public List<Article> getAllArticles() throws CustomException {
		if (articleRepository.findAll().isEmpty()) {
			throw new CustomException("Article list is empty!!!");
		}
		return articleRepository.findAll();
	}

	@Override
	public Article getArticleById(String slug) throws CustomException {
		Optional<Article> optionalArticle = articleRepository.findBySlug(slug);
		if (!optionalArticle.isPresent()) {
			throw new CustomException("Aricle does not exists");
		}
		Article article = new Article();
		BeanUtils.copyProperties(optionalArticle.get(), article);
		return article;
	}

	@Override
	public String deleteArticleById(String slug) throws CustomException {
		Optional<Article> optionalArticle = articleRepository.findBySlug(slug);
		if (!optionalArticle.isPresent()) {
			throw new CustomException("Aricle does not exists");
		}
		articleRepository.delete(optionalArticle.get());
		return optionalArticle.get().getTitle() + " Article Deleted Successfully";
	}

	@Override
	public String updateArticle(Article article) throws CustomException {
		Optional<Article> optionalArticle = articleRepository.findBySlug(article.getSlug());
		if (!optionalArticle.isPresent()) {
			throw new CustomException("Aricle does not exists");
		}
		articleRepository.save(article);

		return article.getTitle() + " Article updated Successfully";

	}

}
