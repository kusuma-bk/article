package com.org.article.dto;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ArticleRequestDto {
	@NotEmpty(message = "Title  should not be empty")
	private String title;
	@NotEmpty(message = "Description should not be empty")
	private String description;
	@NotEmpty(message = "Body should not be empty")
	private String body;
	@NotEmpty(message = "Body should not be empty")
	private String slug;

}
