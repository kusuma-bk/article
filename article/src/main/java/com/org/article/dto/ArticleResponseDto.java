package com.org.article.dto;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
public class ArticleResponseDto {
	private String articleId;
	private String slug;
	private String title;
	private String description;
	private String body;
	private String[] tags;
	private LocalDateTime createdAt;
	private LocalDateTime updatedAt;
	private boolean favorited;
	private int favoritesCount;

}
