package com.org.article.controller;

import java.util.List;

import javax.validation.Valid;

import org.omg.CORBA.UserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.org.article.dto.ArticleRequestDto;
import com.org.article.entity.Article;
import com.org.article.exception.CustomException;
import com.org.article.service.ArticleService;

@RestController
@RequestMapping("/api/articles")
public class ArticleController {

	@Autowired
	ArticleService articleService;

	@PostMapping
	ResponseEntity<Article> createArticle(@Valid @RequestBody ArticleRequestDto articleRequestDto,
			@RequestParam(required = false) String[] tags) throws CustomException {
		return new ResponseEntity<>(articleService.createArticle(articleRequestDto, tags), HttpStatus.CREATED);
	}

	@GetMapping
	ResponseEntity<List<Article>> getAllArticles() throws CustomException {
		return new ResponseEntity<>(articleService.getAllArticles(), HttpStatus.OK);
	}

	@GetMapping("/{slug}")
	ResponseEntity<Article> getArticleById(@PathVariable String slug) throws CustomException {
		return new ResponseEntity<>(articleService.getArticleById(slug), HttpStatus.OK);
	}

	@DeleteMapping("/{slug}")
	ResponseEntity<String> deleteArticleById(@PathVariable String slug) throws CustomException {
		return new ResponseEntity<>(articleService.deleteArticleById(slug), HttpStatus.OK);
	}

	@PatchMapping
	ResponseEntity<String> updateArticle(@RequestBody Article article) throws CustomException {
		return new ResponseEntity<>(articleService.updateArticle(article), HttpStatus.OK);
	}

}
