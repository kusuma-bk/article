package com.org.article.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter
@Getter
@ToString
public class Article {
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	private String articleId;
	private String slug;
	private String title;
	private String description;
	private String body;
	private String[] tags;
	private LocalDateTime createdAt;
	private LocalDateTime updatedAt;
	private boolean favorited;
	private int favoritesCount;

}
